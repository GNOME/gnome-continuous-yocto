# Yocto base for GNOME Continuous

This repository contains the base OS required to build GNOME Continuous from
scratch.

We use Yocto as a maintained, vendor-neutral base for Continuous.

### Update policy

The Yocto base should be updated regularly, in order to keep it in sync with
upstream. This allows us to rely on bug fixes and security updates.

Ideally, we should rebase with every new stable Yocto release, but given the
long term support of each, we can also merge the changes from a stable
branch.

For the release activity of Yocto, please [see this page](https://wiki.yoctoproject.org/wiki/Releases).

To update, just merge the current tip of the Yocto branch in use onto the
branch of the gnome-continuous-yocto repository:

```sh
$ git checkout gnomeostree-3.22-krogoth
$ git merge yocto/krogoth
```

### Rebasing

The process of rebasing gnome-continuous-yocto is not entirely trivial.

First of all, you will need an empty branch; the naming policy is to use the
latest version of GNOME at the time of the rebase, and the code name for the
Yocto release in use, for instance:

  - gnomeostree-3.14-dizzy
  - gnomeostree-3.22-krogoth

The new branch must import the original Yocto repository, and point at the
necessary branch. For instance, if we want to rebase on top of Yocto 2.4,
code name "Rocko", for GNOME 3.28:

```sh
# Ensure we're on master
$ git checkout master

# Fetch the current state of the Yocto repository
$ git remote add yocto https://git.yoctoproject.org/git/poky
$ git fetch yocto

# Create the new branch
$ git checkout --orphan gnomeostree-3.28-rocko
$ git merge yocto/rocko
```

At this point, you'll have the README and DOAP file in the Git staging area;
commit them as a marker.

```sh
$ git commit -m "continuous: Rebase on Yocto 2.4/Rocko"
```

Now comes the hard part.

We cannot merge the current stable branch of gnome-continuous-yocto, as they
will conflict with the changes in the Yocto repository. Instead, we simply
clone gnome-continuous-yocto again elsewhere, and we copy the following
files and directories:

  * [LAYERS](./LAYERS)
  * [meta-gnomeos](./meta-gnomeos)

You may need to modify the `.gitignore` file.

After importing the Continuous-related files, you will need to add the
meta-openembedded recipes as a submodule 

```sh
$ git submodule add --name meta-oe https://github.com/openembedded/meta-openembedded meta-oe
```

Make sure you change the branch of the meta-openembedded recipes so that
they match the branch of Yocto you are rebasing to; mixing Yocto and
OpenEmbedded branches is typically a recipe for a hot mess:

```sh
$ git config -f .gitmodules submodule.meta-oe.branch rocko
```

Now you can update the meta-oe submodule using:

```sh
$ git submodule update --remote meta-oe
```

Now you will have to go through the various recipes under meta-gnomeos, and
remove the ones that were superceded by the Yocto core and OpenEmbedded
ones.

At this point, you should be done. Push everything to the remote Git
repository, modify the `manifest.json` file in gnome-continuous to point
to the new branch of gnome-continuous-yocto, and wait until the base is
rebuilt.

If the build does not succeed, you'll have to debug the Yocto build, which
is not really fun. Ideally, you could run the build locally, but you will
need to generate the Yocto configuration in the same way gnome-continuous
does, by checking the `ostree-build-yocto` script.
